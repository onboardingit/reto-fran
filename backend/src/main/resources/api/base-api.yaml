openapi: 3.0.2

info:
  version: '1.0'
  title: Backend Movies API Services
  description: Backend Movies API Services
  contact:
    name: Consultec
    url: 'https://www.consultec-ti.com/'
    email: jnieves@consultec-ti.com

servers:
  - url: 'https://dev.example.com'
    description: Development Server
  - url: 'https://qa.example.com'
    description: QA Server
  - url: 'https://prod.example.com'
    description: Production Server

tags:
  - name: book
  - name: chapter
  - name: character
  - name: movie
  - name: quote

paths:
  '/books':
    get:
      summary: Operation to get all books
      tags:
        - book
      description: Returns all books from the system that the user has access to
      operationId: getBooks
      responses:
        '200':
          description: A list of all books.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Book'
              examples:
                books:
                  $ref: '#/components/examples/books'

  '/books/{idBook}':
    get:
      summary: Operation to get a book for ID
      tags:
        - book
      description: Returns a system book by numeric identifier
      operationId: getBookById
      parameters:
        - name: idBook
          in: path
          description: ID of the book to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a book.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Book'
              examples:
                book:
                  $ref: '#/components/examples/book'
        '400':
          description: Invalid ID supplied
        '404':
          description: Book not found

  '/books/{idBook}/chapter':
    get:
      summary: Operation to obtain all chapters by identifier number of a book
      tags:
        - book
      description: Returns a list of chapters by identifier number of a book
      operationId: getChaptersByBookID
      parameters:
        - name: idBook
          in: path
          description: ID of the book to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: A list of chapters.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Chapter'
              examples:
                chapters:
                  $ref: '#/components/examples/chaptersByBookID'
        '400':
          description: Invalid ID supplied
        '404':
          description: Book not found

  '/chapters':
    get:
      summary: Operation to get all chapters of all of books
      tags:
        - chapter
      description: Returns a list chapters
      operationId: getChapters
      responses:
        '200':
          description: A list of all the chapters.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Chapter'
              examples:
                chapters:
                  $ref: '#/components/examples/chaptersByBookID'

  '/chapters/{idChapter}':
    get:
      summary: Operation to get a chapter for ID
      tags:
        - chapter
      description: Returns a system chapter by numeric identifier
      operationId: getChapterById
      parameters:
        - name: idChapter
          in: path
          description: ID of the chapter to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a chapter.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Chapter'
              examples:
                book:
                  $ref: '#/components/examples/chapters'
        '400':
          description: Invalid ID supplied
        '404':
          description: Chapter not found

  '/characters':
    get:
      summary: Operation to get all characters of all of books and movies
      tags:
        - character
      description: Returns a list characters
      operationId: getCharacters
      responses:
        '200':
          description: A list of all the characters.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Character'
              examples:
                chapters:
                  $ref: '#/components/examples/characters'

  '/characters/{idCharacter}':
    get:
      summary: Operation to get a character for ID
      tags:
        - character
      description: Returns a system character by numeric identifier
      operationId: getCharacterById
      parameters:
        - name: idCharacter
          in: path
          description: ID of the character to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a character.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Character'
              examples:
                book:
                  $ref: '#/components/examples/character'
        '400':
          description: Invalid ID supplied
        '404':
          description: Character not found

  '/characters/{idCharacter}/quote':
    get:
      summary: Operation to get a quote for character ID
      tags:
        - character
      description: Returns a system quote by numeric identifier of character
      operationId: getQuoteByCharacterId
      parameters:
        - name: idCharacter
          in: path
          description: ID of the character to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a character.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Quote'
              examples:
                book:
                  $ref: '#/components/examples/quotesbyCharaterID'
        '400':
          description: Invalid ID supplied
        '404':
          description: Character not found

  '/movies':
    get:
      summary: Operation to get all movies
      tags:
        - movie
      description: Returns a list movies
      operationId: getMovies
      responses:
        '200':
          description: A list of all the movies.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Movie'
              examples:
                chapters:
                  $ref: '#/components/examples/movies'

  '/movies/{idMovie}':
    get:
      summary: Operation to get a movie for ID
      tags:
        - movie
      description: Returns a system movie by numeric identifier
      operationId: getMovieById
      parameters:
        - name: idMovie
          in: path
          description: ID of the movie to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a movie.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Movie'
              examples:
                book:
                  $ref: '#/components/examples/movie'
        '400':
          description: Invalid ID supplied
        '404':
          description: Film not found

  '/movies/{idMovie}/quote':
    get:
      summary: Operation to get a quote for film ID
      tags:
        - movie
      description: Returns a system quote by numeric identifier of film
      operationId: getQuoteByMovieId
      parameters:
        - name: idMovie
          in: path
          description: ID of the film to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a film.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Quote'
              examples:
                book:
                  $ref: '#/components/examples/quotesbyFilmID'
        '400':
          description: Invalid ID supplied
        '404':
          description: Film not found

  '/quote':
    get:
      summary: Operation to get all quotes of all of books and movies
      tags:
        - quote
      description: Returns a list quotes
      operationId: getQuotes
      responses:
        '200':
          description: A list of all the quotes.
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Quote'
              examples:
                chapters:
                  $ref: '#/components/examples/quotes'

  '/quote/{idQuote}':
    get:
      summary: Operation to get a quote for ID
      tags:
        - quote
      description: Returns a system quote by numeric identifier
      operationId: getQuoteById
      parameters:
        - name: idQuote
          in: path
          description: ID of the quote to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: Information of a quote.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Quote'
              examples:
                book:
                  $ref: '#/components/examples/quote'
        '400':
          description: Invalid ID supplied
        '404':
          description: Quote not found


components:
  schemas:
    Book:
      description: Entity that contains the characteristic of a book.
      type: object
      properties:
        id:
          description: book identifier number.
          type: string
          example: 5cd99d4bde30eff6ebccfe9e
        name:
          description: Name or title of the book.
          type: string
          example: The Fellowship Of The Ring
    Chapter:
      description: Entity that contains the chapter entity of a book.
      type: object
      properties:
        id:
          description: Chapter identifier number.
          type: string
          example: 5cd99d4bde30eff6ebccfe9e
        chapterName:
          description: Name or title the chapter in the book.
          type: string
          example: A Long-expected Party
        book:
          $ref: '#/components/schemas/Book'
    Character:
      description: Entity that contains information the character of a book.
      type: object
      properties:
        id:
          description: Character identifier number.
          type: string
          example: 5cd99d4bde30eff6ebccfe9e
        name:
          description: Name or title of the character.
          type: string
          example: Aragorn I
        race:
          description: Race or ethnicity or title of the character
          type: string
          example: Human
        gender:
          description: Sexual gender of the character
          type: string
          example: Male
          enum:
            - Female
            - Male
        hair:
          description: Colour of hair of the character
          type: string
          example: Dark brown
        height:
          description: height of the character.
          type: string
          example: TA 2227
        birth:
          description: place o date of birth of a character.
          type: string
          example: TA 2227
        death:
          description: place o date of death of a character.
          type: string
          example: Late ,Third Age
        realm:
          description: place or realm where the character lived .
          type: string
          example: Arthedain
        spouse:
          description: character couple.
          type: string
          example: Belemir
        wikiUrl:
          description: Access link for see information of a character.
          type: string
          example: 'http://lotr.wikia.com//wiki/Aragorn_I'
    Movie:
      description: Entity that contains information of a film.
      type: object
      properties:
        id:
          description: Film identifier number.
          type: string
          example: 5cd99d4bde30eff6ebccfe9e
        name:
          description: Name or title of the film.
          type: string
          example: The Lord of the Rings Series
        runtimeInMinutes:
          description: Time o duration in minutes of a film.
          type: number
          example: 558
        budgetInMillions:
          description: Money invested in the production of a film.
          type: number
          format: double
          example: 281
        boxOfficeRevenueInMillions:
          description: Money earned on film by appearing at the cinema.
          type: number
          format: double
          example: 871.5
        academyAwardNominations:
          description: Number of award nominations from a film.
          type: number
          example: 13
        academyAwardWins:
          description: Number of award wins from a film.
          type: number
          example: 17
        rottenTomatesScore:
          description: Score of the critical on Rotten Tomatoes.
          type: number
          format: double
          example: 66.33
    Quote:
      description: Entity that contains a quote of a film.
      type: object
      properties:
        id:
          description: Quote identifier number.
          type: string
          example: 5cd99d4bde30eff6ebccfe9e
        dialog:
          description: Quote of a dialog in a film.
          type: string
          example: Free? He will never be free!
        movie:
          $ref: '#/components/schemas/Movie'
        character:
          $ref: '#/components/schemas/Character'

  examples:
    book:
      summary: Example to get a book
      value:
        id: 5cd99d4bde30eff6ebccfe9e
        name: The Fellowship Of The Ring

    books:
      summary: Example to get a list of all the books
      value:
        - id: 5cd99d4bde30eff6ebccfe9e
          name: The Fellowship Of The Ring
        - id: 5cd99d4bde30eff6ebccfe9a
          name: The Two Towers

    chapter:
      summary: Example to get a chapter of the a book
      value:
        id: 5cd99d4bde30eff6ebccfe33
        name: A Long-expected Party
        book:
          id: 5cd99d4bde30eff6ebccfe23

    chapters:
      summary: Example to get the lists of chapters of all the books
      value:
        - id: 5cd99d4bde30eff6ebccfe9e
          name: A Long-expected Party
          book:
            id: 5cd99d4bde30eff6ebccfs89
        - id: 5cd99d4bde30eff6ebccfs87
          name: A Long-expected Party
          book:
            id: 5cd99d4bde30eff6ebccfw78

    chaptersByBookID:
      summary: Example to get the lists of chapters of all the books
      value:
        - id: 5cd99d4bde30eff6ebccfw78
          name: A Long-expected Party
          book:
            id: 456789
        - id: 5cd99d4bde30ess6ebccfw78
          name: A Long-expected Party
          book:
            id: 456789

    character:
      summary: Example of a characters
      value:
        id: 5cd99d4bde30ess6ebccfw78
        name: Aragorn I
        race: Human
        gender: Male
        hair: Dark brown
        height: Tall
        realm: Rohan
        spouse: Unnamed wife
        birth: TA 2227
        death: Late ,Third Age
        wikiUrl: 'http://lotr.wikia.com//wiki/Aragorn_I'

    characters:
      summary: Example of a list of characters
      value:
        - id: 5cd99d4bde30ess6ebccfw78
          name: Aragorn I
          race: Human
          gender: Male
          hair: Dark brown
          height: Tall
          realm: Rohan
          spouse: Unnamed wife
          birth: TA 2227
          death: Late ,Third Age
          wikiUrl: 'http://lotr.wikia.com//wiki/Aragorn_I'
        - id: 5cd99d4bde30ess6ebcc23ds
          name: Celebrimbor I
          race: Human
          gender: Male
          hair: Dark brown
          height: 198cm (6'6"
          realm: Rohan
          spouse: Belemir
          birth: TA 2227
          death: SA 3434
          wikiUrl: 'http://lotr.wikia.com//wiki/Aragorn_I'

    movie:
      summary: Example of a film
      value:
        id: 5cd99d4bde30ess6ebccfw78
        name: The Fellowship Of The Ring
        runtimeInMinutes: 558
        budgetInMillions: 281
        boxOfficeRevenueInMillions: 871.5
        academyAwardNominations: 13
        academyAwardWins: 17
        rottenTomatesScore: 66.33

    movies:
      summary: Example of a list of movies
      value:
        - id: 5cd99d4bde30ess6ebccfw78
          name: The Fellowship Of The Ring
          runtimeInMinutes: 558
          budgetInMillions: 281
          boxOfficeRevenueInMillions: 871.5
          academyAwardNominations: 13
          academyAwardWins: 17
          rottenTomatesScore: 66.33
        - id: 5cd99d4bde30ess6ebccer1d
          name: The Fellowship Of The Ring
          runtimeInMinutes: 558
          budgetInMillions: 281
          boxOfficeRevenueInMillions: 871.5
          academyAwardNominations: 13
          academyAwardWins: 17
          rottenTomatesScore: 66.33

    quote:
      summary: Example of a quote
      value:
        id: 5cd99d4bde30ess6ebcc23ds
        dialog: Free? He will never be free!
        movie:
          id: '5cd99d4bde30ess6ebccer1d'
        character:
          id: '5cd99d4bde30ess6ebccer1s'

    quotes:
      summary: Example of a list of quotes
      value:
        - id: 5cd99d4bde30ess6ebcc23ds
          dialog: Free? He will never be free!
          movie:
            id: '5cd99d4bde30ess6ebccer14f'
          character:
            id: '5cd99d4bde30ess6ebccer3e'
        - id: 5cd99d4bde30ess64bcc23ds
          dialog: Free? He will never be free!
          movie:
            id: '5cd99d4bde30ess6eccer14f'
          character:
            id: '5cd99d4bde30ess6ebccer3e'

    quotesbyFilmID:
      summary: Example of a list of quotes for Film ID
      value:
        - id: 5cd99d4bde30ess6ebcc23ds
          dialog: Free? He will never be free!
          movie:
            id: '5cd99d4bde30ess6ebccer14f'
          character:
            id: '5cd99d4bde30ess6ebccer3e'
        - id: 5cd99d4bde30ess64bcc23ds
          dialog: Free? He will never be free!
          movie:
            id: '5cd99d4bde30ess6eccer14f'
          character:
            id: '5cd99d4bde30ess6ebccer3e'

    quotesbyCharaterID:
      summary: Example of a list of quotes for Charater ID
      value:
        - id: 5cd99d4bde30ess6ebcc23ds
          dialog: Free? He will never be free!
          movie:
            id: '5cd99d4bde30ess6ebccer14f'
          character:
            id: '5cd99d4bde30ess6ebccer3e'
        - id: 5cd99d4bde30ess64bcc23ds
          dialog: Free? He will never be free!
          movie:
            id: '5cd99d4bde30ess6eccer14f'
          character:
            id: '5cd99d4bde30ess6ebccer3e'
