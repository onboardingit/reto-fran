package com.consultec.training.oneapibackend.controller.model;

import java.util.Objects;
import com.consultec.training.oneapibackend.controller.model.Character;
import com.consultec.training.oneapibackend.controller.model.Movie;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Entity that contains a quote of a film.
 */
@Schema(description = "Entity that contains a quote of a film.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T07:25:54.642734-05:00[America/Bogota]")


public class Quote   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("dialog")
  private String dialog = null;

  @JsonProperty("movie")
  private Movie movie = null;

  @JsonProperty("character")
  private Character character = null;

  public Quote id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Quote identifier number.
   * @return id
   **/
  @Schema(example = "5cd99d4bde30eff6ebccfe9e", description = "Quote identifier number.")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Quote dialog(String dialog) {
    this.dialog = dialog;
    return this;
  }

  /**
   * Quote of a dialog in a film.
   * @return dialog
   **/
  @Schema(example = "Free? He will never be free!", description = "Quote of a dialog in a film.")
  
    public String getDialog() {
    return dialog;
  }

  public void setDialog(String dialog) {
    this.dialog = dialog;
  }

  public Quote movie(Movie movie) {
    this.movie = movie;
    return this;
  }

  /**
   * Get movie
   * @return movie
   **/
  @Schema(description = "")
  
    @Valid
    public Movie getMovie() {
    return movie;
  }

  public void setMovie(Movie movie) {
    this.movie = movie;
  }

  public Quote character(Character character) {
    this.character = character;
    return this;
  }

  /**
   * Get character
   * @return character
   **/
  @Schema(description = "")
  
    @Valid
    public Character getCharacter() {
    return character;
  }

  public void setCharacter(Character character) {
    this.character = character;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Quote quote = (Quote) o;
    return Objects.equals(this.id, quote.id) &&
        Objects.equals(this.dialog, quote.dialog) &&
        Objects.equals(this.movie, quote.movie) &&
        Objects.equals(this.character, quote.character);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, dialog, movie, character);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Quote {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dialog: ").append(toIndentedString(dialog)).append("\n");
    sb.append("    movie: ").append(toIndentedString(movie)).append("\n");
    sb.append("    character: ").append(toIndentedString(character)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
