package com.consultec.training.oneapibackend.repository.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "users")
public class Users  implements Serializable {

    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

    @Column(name = "email")
    private String email;

    @Column(name = "lastsession")
    private Date lastsession;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.password);
        hash = 79 * hash + Objects.hashCode(this.email);
        hash = 79 * hash + Objects.hashCode(this.lastsession);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Users other = (Users) obj;

        if (!Objects.equals(this.name, other.name)) {
            return false;
        }if (!Objects.equals(this.password, other.password)) {
            return false;
        }if (!Objects.equals(this.email, other.email)) {
            return false;
        }if (!Objects.equals(this.lastsession, other.lastsession)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("City{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", name='").append(password).append('\'');
        sb.append(", name='").append(email).append('\'');
        sb.append(", name='").append(lastsession).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
