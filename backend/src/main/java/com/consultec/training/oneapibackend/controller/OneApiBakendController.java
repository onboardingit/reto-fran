package com.consultec.training.oneapibackend.controller;

import com.consultec.training.oneapibackend.controller.api.*;
import com.consultec.training.oneapibackend.controller.model.*;
import com.consultec.training.oneapibackend.controller.model.Character;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping
public class OneApiBakendController implements BooksApi, ChaptersApi, CharactersApi, MoviesApi, QuoteApi {


    @Override
    public ResponseEntity<List<Book>> getBooks() {
        return null;
    }

    @Override
    public ResponseEntity<Book> getBookById(String idBook) {
        return null;
    }

    @Override
    public ResponseEntity<List<Chapter>> getChaptersByBookID(String idBook) {
        return null;
    }

    @Override
    public ResponseEntity<List<Chapter>> getChapters() {
        return null;
    }

    @Override
    public ResponseEntity<Chapter> getChapterById(String idChapter) {
        return null;
    }

    @Override
    public ResponseEntity<List<Character>> getCharacters() {
        return null;
    }

    @Override
    public ResponseEntity<Character> getCharacterById(String idCharacter) {
        return null;
    }

    @Override
    public ResponseEntity<List<Quote>> getQuoteByCharacterId(String idCharacter) {
        return null;
    }

    @Override
    public ResponseEntity<List<Movie>> getMovies() {
        return null;
    }

    @Override
    public ResponseEntity<Movie> getMovieById(String idMovie) {
        return null;
    }

    @Override
    public ResponseEntity<List<Quote>> getQuoteByMovieId(String idMovie) {
        return null;
    }

    @Override
    public ResponseEntity<List<Quote>> getQuotes() {
        return null;
    }

    @Override
    public ResponseEntity<Quote> getQuoteById(String idQuote) {
        return null;
    }
}
