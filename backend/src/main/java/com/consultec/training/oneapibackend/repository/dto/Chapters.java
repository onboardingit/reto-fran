package com.consultec.training.oneapibackend.repository.dto;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Entity
@Table(name = "chapters", uniqueConstraints = {
        @UniqueConstraint(columnNames = "book") })
public class Chapters implements Serializable {

    @Id
    private String id;

    @Column(name = "chapterName")
    private String chapterName;

    @OneToOne
    @JoinColumn(name="id", nullable=false)
    private Books book;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.chapterName);
        hash = 79 * hash + this.book.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Chapters other = (Chapters) obj;

        if (!Objects.equals(this.chapterName, other.chapterName)) {
            return false;
        }

        if (!Objects.equals(this.book, other.book)) {
            return false;
        }

        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("City{");
        sb.append("id=").append(id);
        sb.append(", name='").append(chapterName).append('\'');
        sb.append(", name='").append(book).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
