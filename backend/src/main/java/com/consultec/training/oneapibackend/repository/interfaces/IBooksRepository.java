package com.consultec.training.oneapibackend.repository.interfaces;

import com.consultec.training.oneapibackend.repository.dto.Books;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBooksRepository extends CrudRepository <Books, String> {

}
