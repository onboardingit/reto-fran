package com.consultec.training.oneapibackend.controller.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Entity that contains information of a film.
 */
@Schema(description = "Entity that contains information of a film.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T07:25:54.642734-05:00[America/Bogota]")


public class Movie   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("runtimeInMinutes")
  private BigDecimal runtimeInMinutes = null;

  @JsonProperty("budgetInMillions")
  private Double budgetInMillions = null;

  @JsonProperty("boxOfficeRevenueInMillions")
  private Double boxOfficeRevenueInMillions = null;

  @JsonProperty("academyAwardNominations")
  private BigDecimal academyAwardNominations = null;

  @JsonProperty("academyAwardWins")
  private BigDecimal academyAwardWins = null;

  @JsonProperty("rottenTomatesScore")
  private Double rottenTomatesScore = null;

  public Movie id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Film identifier number.
   * @return id
   **/
  @Schema(example = "5cd99d4bde30eff6ebccfe9e", description = "Film identifier number.")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Movie name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name or title of the film.
   * @return name
   **/
  @Schema(example = "The Lord of the Rings Series", description = "Name or title of the film.")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Movie runtimeInMinutes(BigDecimal runtimeInMinutes) {
    this.runtimeInMinutes = runtimeInMinutes;
    return this;
  }

  /**
   * Time o duration in minutes of a film.
   * @return runtimeInMinutes
   **/
  @Schema(example = "558", description = "Time o duration in minutes of a film.")
  
    @Valid
    public BigDecimal getRuntimeInMinutes() {
    return runtimeInMinutes;
  }

  public void setRuntimeInMinutes(BigDecimal runtimeInMinutes) {
    this.runtimeInMinutes = runtimeInMinutes;
  }

  public Movie budgetInMillions(Double budgetInMillions) {
    this.budgetInMillions = budgetInMillions;
    return this;
  }

  /**
   * Money invested in the production of a film.
   * @return budgetInMillions
   **/
  @Schema(example = "281", description = "Money invested in the production of a film.")
  
    public Double getBudgetInMillions() {
    return budgetInMillions;
  }

  public void setBudgetInMillions(Double budgetInMillions) {
    this.budgetInMillions = budgetInMillions;
  }

  public Movie boxOfficeRevenueInMillions(Double boxOfficeRevenueInMillions) {
    this.boxOfficeRevenueInMillions = boxOfficeRevenueInMillions;
    return this;
  }

  /**
   * Money earned on film by appearing at the cinema.
   * @return boxOfficeRevenueInMillions
   **/
  @Schema(example = "871.5", description = "Money earned on film by appearing at the cinema.")
  
    public Double getBoxOfficeRevenueInMillions() {
    return boxOfficeRevenueInMillions;
  }

  public void setBoxOfficeRevenueInMillions(Double boxOfficeRevenueInMillions) {
    this.boxOfficeRevenueInMillions = boxOfficeRevenueInMillions;
  }

  public Movie academyAwardNominations(BigDecimal academyAwardNominations) {
    this.academyAwardNominations = academyAwardNominations;
    return this;
  }

  /**
   * Number of award nominations from a film.
   * @return academyAwardNominations
   **/
  @Schema(example = "13", description = "Number of award nominations from a film.")
  
    @Valid
    public BigDecimal getAcademyAwardNominations() {
    return academyAwardNominations;
  }

  public void setAcademyAwardNominations(BigDecimal academyAwardNominations) {
    this.academyAwardNominations = academyAwardNominations;
  }

  public Movie academyAwardWins(BigDecimal academyAwardWins) {
    this.academyAwardWins = academyAwardWins;
    return this;
  }

  /**
   * Number of award wins from a film.
   * @return academyAwardWins
   **/
  @Schema(example = "17", description = "Number of award wins from a film.")
  
    @Valid
    public BigDecimal getAcademyAwardWins() {
    return academyAwardWins;
  }

  public void setAcademyAwardWins(BigDecimal academyAwardWins) {
    this.academyAwardWins = academyAwardWins;
  }

  public Movie rottenTomatesScore(Double rottenTomatesScore) {
    this.rottenTomatesScore = rottenTomatesScore;
    return this;
  }

  /**
   * Score of the critical on Rotten Tomatoes.
   * @return rottenTomatesScore
   **/
  @Schema(example = "66.33", description = "Score of the critical on Rotten Tomatoes.")
  
    public Double getRottenTomatesScore() {
    return rottenTomatesScore;
  }

  public void setRottenTomatesScore(Double rottenTomatesScore) {
    this.rottenTomatesScore = rottenTomatesScore;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Movie movie = (Movie) o;
    return Objects.equals(this.id, movie.id) &&
        Objects.equals(this.name, movie.name) &&
        Objects.equals(this.runtimeInMinutes, movie.runtimeInMinutes) &&
        Objects.equals(this.budgetInMillions, movie.budgetInMillions) &&
        Objects.equals(this.boxOfficeRevenueInMillions, movie.boxOfficeRevenueInMillions) &&
        Objects.equals(this.academyAwardNominations, movie.academyAwardNominations) &&
        Objects.equals(this.academyAwardWins, movie.academyAwardWins) &&
        Objects.equals(this.rottenTomatesScore, movie.rottenTomatesScore);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, runtimeInMinutes, budgetInMillions, boxOfficeRevenueInMillions, academyAwardNominations, academyAwardWins, rottenTomatesScore);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Movie {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    runtimeInMinutes: ").append(toIndentedString(runtimeInMinutes)).append("\n");
    sb.append("    budgetInMillions: ").append(toIndentedString(budgetInMillions)).append("\n");
    sb.append("    boxOfficeRevenueInMillions: ").append(toIndentedString(boxOfficeRevenueInMillions)).append("\n");
    sb.append("    academyAwardNominations: ").append(toIndentedString(academyAwardNominations)).append("\n");
    sb.append("    academyAwardWins: ").append(toIndentedString(academyAwardWins)).append("\n");
    sb.append("    rottenTomatesScore: ").append(toIndentedString(rottenTomatesScore)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
