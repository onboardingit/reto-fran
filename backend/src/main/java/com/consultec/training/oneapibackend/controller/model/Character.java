package com.consultec.training.oneapibackend.controller.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Entity that contains information the character of a book.
 */
@Schema(description = "Entity that contains information the character of a book.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T07:25:54.642734-05:00[America/Bogota]")


public class Character   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @JsonProperty("race")
  private String race = null;

  /**
   * Sexual gender of the character
   */
  public enum GenderEnum {
    FEMALE("Female"),
    
    MALE("Male");

    private String value;

    GenderEnum(String value) {
      this.value = value;
    }

    @Override
    @JsonValue
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static GenderEnum fromValue(String text) {
      for (GenderEnum b : GenderEnum.values()) {
        if (String.valueOf(b.value).equals(text)) {
          return b;
        }
      }
      return null;
    }
  }
  @JsonProperty("gender")
  private GenderEnum gender = null;

  @JsonProperty("hair")
  private String hair = null;

  @JsonProperty("height")
  private String height = null;

  @JsonProperty("birth")
  private String birth = null;

  @JsonProperty("death")
  private String death = null;

  @JsonProperty("realm")
  private String realm = null;

  @JsonProperty("spouse")
  private String spouse = null;

  @JsonProperty("wikiUrl")
  private String wikiUrl = null;

  public Character id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Character identifier number.
   * @return id
   **/
  @Schema(example = "5cd99d4bde30eff6ebccfe9e", description = "Character identifier number.")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Character name(String name) {
    this.name = name;
    return this;
  }

  /**
   * Name or title of the character.
   * @return name
   **/
  @Schema(example = "Aragorn I", description = "Name or title of the character.")
  
    public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Character race(String race) {
    this.race = race;
    return this;
  }

  /**
   * Race or ethnicity or title of the character
   * @return race
   **/
  @Schema(example = "Human", description = "Race or ethnicity or title of the character")
  
    public String getRace() {
    return race;
  }

  public void setRace(String race) {
    this.race = race;
  }

  public Character gender(GenderEnum gender) {
    this.gender = gender;
    return this;
  }

  /**
   * Sexual gender of the character
   * @return gender
   **/
  @Schema(example = "Male", description = "Sexual gender of the character")
  
    public GenderEnum getGender() {
    return gender;
  }

  public void setGender(GenderEnum gender) {
    this.gender = gender;
  }

  public Character hair(String hair) {
    this.hair = hair;
    return this;
  }

  /**
   * Colour of hair of the character
   * @return hair
   **/
  @Schema(example = "Dark brown", description = "Colour of hair of the character")
  
    public String getHair() {
    return hair;
  }

  public void setHair(String hair) {
    this.hair = hair;
  }

  public Character height(String height) {
    this.height = height;
    return this;
  }

  /**
   * height of the character.
   * @return height
   **/
  @Schema(example = "TA 2227", description = "height of the character.")
  
    public String getHeight() {
    return height;
  }

  public void setHeight(String height) {
    this.height = height;
  }

  public Character birth(String birth) {
    this.birth = birth;
    return this;
  }

  /**
   * place o date of birth of a character.
   * @return birth
   **/
  @Schema(example = "TA 2227", description = "place o date of birth of a character.")
  
    public String getBirth() {
    return birth;
  }

  public void setBirth(String birth) {
    this.birth = birth;
  }

  public Character death(String death) {
    this.death = death;
    return this;
  }

  /**
   * place o date of death of a character.
   * @return death
   **/
  @Schema(example = "Late ,Third Age", description = "place o date of death of a character.")
  
    public String getDeath() {
    return death;
  }

  public void setDeath(String death) {
    this.death = death;
  }

  public Character realm(String realm) {
    this.realm = realm;
    return this;
  }

  /**
   * place or realm where the character lived .
   * @return realm
   **/
  @Schema(example = "Arthedain", description = "place or realm where the character lived .")
  
    public String getRealm() {
    return realm;
  }

  public void setRealm(String realm) {
    this.realm = realm;
  }

  public Character spouse(String spouse) {
    this.spouse = spouse;
    return this;
  }

  /**
   * character couple.
   * @return spouse
   **/
  @Schema(example = "Belemir", description = "character couple.")
  
    public String getSpouse() {
    return spouse;
  }

  public void setSpouse(String spouse) {
    this.spouse = spouse;
  }

  public Character wikiUrl(String wikiUrl) {
    this.wikiUrl = wikiUrl;
    return this;
  }

  /**
   * Access link for see information of a character.
   * @return wikiUrl
   **/
  @Schema(example = "http://lotr.wikia.com//wiki/Aragorn_I", description = "Access link for see information of a character.")
  
    public String getWikiUrl() {
    return wikiUrl;
  }

  public void setWikiUrl(String wikiUrl) {
    this.wikiUrl = wikiUrl;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Character character = (Character) o;
    return Objects.equals(this.id, character.id) &&
        Objects.equals(this.name, character.name) &&
        Objects.equals(this.race, character.race) &&
        Objects.equals(this.gender, character.gender) &&
        Objects.equals(this.hair, character.hair) &&
        Objects.equals(this.height, character.height) &&
        Objects.equals(this.birth, character.birth) &&
        Objects.equals(this.death, character.death) &&
        Objects.equals(this.realm, character.realm) &&
        Objects.equals(this.spouse, character.spouse) &&
        Objects.equals(this.wikiUrl, character.wikiUrl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, race, gender, hair, height, birth, death, realm, spouse, wikiUrl);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Character {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    race: ").append(toIndentedString(race)).append("\n");
    sb.append("    gender: ").append(toIndentedString(gender)).append("\n");
    sb.append("    hair: ").append(toIndentedString(hair)).append("\n");
    sb.append("    height: ").append(toIndentedString(height)).append("\n");
    sb.append("    birth: ").append(toIndentedString(birth)).append("\n");
    sb.append("    death: ").append(toIndentedString(death)).append("\n");
    sb.append("    realm: ").append(toIndentedString(realm)).append("\n");
    sb.append("    spouse: ").append(toIndentedString(spouse)).append("\n");
    sb.append("    wikiUrl: ").append(toIndentedString(wikiUrl)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
