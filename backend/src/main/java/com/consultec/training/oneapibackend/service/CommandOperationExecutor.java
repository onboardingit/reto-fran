package com.consultec.training.oneapibackend.service;

import com.consultec.training.oneapibackend.controller.exception.BusinessException;
import org.springframework.stereotype.Service;

import java.io.Serializable;

@Service
public class CommandOperationExecutor {
    public Serializable executeOperation(IAmCommand operation, Serializable dto) throws BusinessException {
        return operation.execute(dto);
    }
}
