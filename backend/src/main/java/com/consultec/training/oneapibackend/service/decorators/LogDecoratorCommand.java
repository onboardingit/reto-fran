package com.consultec.training.oneapibackend.service.decorators;

import com.consultec.training.oneapibackend.controller.exception.BusinessException;
import com.consultec.training.oneapibackend.service.CommandOperationExecutor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.io.Serializable;
import java.util.UUID;

/**
 * Command decorador que implementa log para los mensajes de entrada y salida equivalente al objeto DTO
 *
 * @author Francisco <jnieves@consultec-ti.com>
 *
 * @param <O> BaseDTO<O>: <O> corresponde al cuerpo (body) DTO de salida
 */

public class LogDecoratorCommand <O extends Serializable> implements IAmCommand {
    private final IAmCommand<O> innerHandler;
    private final Logger logger;

    /**
     * @param innerHandler Command encapsulado que ejecutara otra operacion
     */
    public LogDecoratorCommand(IAmCommand<O> innerHandler) {
        logger = LogManager.getLogger("LogDecorator");
        this.innerHandler = innerHandler;
    }

    @Override
    public Serializable execute(Serializable serializable) throws BusinessException {
        ObjectMapper objectMapper =  new ObjectMapper();
        Serializable inBaseDTO = serializable;
        Serializable outbaseDTO = null;

        try{
            ThreadContext.put("requestId", UUID.randomUUID().toString());
            logger.debug("Request: " + objectMapper.writeValueAsString(innerHandler));

            CommandOperationExecutor command = new CommandOperationExecutor();
            outbaseDTO = command.executeOperation(innerHandler, inBaseDTO); // todas la excepciones esperadas de logica de negocio se controlan dentro del comando
            logger.debug("Response: " + objectMapper.writeValueAsString(outbaseDTO));
        }
        catch(BusinessException e) {
            try {
                logger.error("Request", objectMapper.writeValueAsString(outbaseDTO));
                logger.error("Error", e);
            } catch (JsonProcessingException e1) {
                logger.error("Error", e1);
            }
            throw new BusinessException(e.getCode(), e.getMessage(), e.getHttpStatus());
        } catch (JsonProcessingException e) {
            try {
                logger.error("Request", objectMapper.writeValueAsString(outbaseDTO));
                logger.error("Error", e);
            } catch (JsonProcessingException e1) {
                logger.error("Error", e1);
            }
        }

        return outbaseDTO;
    }
}
