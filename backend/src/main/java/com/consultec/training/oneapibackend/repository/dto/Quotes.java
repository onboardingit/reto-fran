package com.consultec.training.oneapibackend.repository.dto;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Data
@Entity
@Table(name = "quotes", uniqueConstraints = {
        @UniqueConstraint(columnNames = "movie"),
        @UniqueConstraint(columnNames = "character") })
public class Quotes implements Serializable {

    @Id
    private String id;

    @Column(name = "dialog")
    private String dialog;

    @OneToOne
    @JoinColumn(name="id", nullable=false)
    private Movies movie;

    @OneToOne
    @JoinColumn(name="id", nullable=false)
    private Characteres character;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.dialog);
        hash = 79 * hash + this.movie.hashCode();
        hash = 79 * hash + this.character.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Quotes other = (Quotes) obj;

        if (!Objects.equals(this.dialog, other.dialog)) {
            return false;
        }

        if (!Objects.equals(this.movie, other.movie)) {
            return false;
        }

        if (!Objects.equals(this.character, other.character)) {
            return false;
        }
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("City{");
        sb.append("id=").append(id);
        sb.append(", name='").append(dialog).append('\'');
        sb.append(", name='").append(movie).append('\'');
        sb.append(", name='").append(character).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
