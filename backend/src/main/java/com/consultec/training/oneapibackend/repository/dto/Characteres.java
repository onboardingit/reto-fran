package com.consultec.training.oneapibackend.repository.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Data
@Entity
@Table(name = "characteres")
public class Characteres implements Serializable {

    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "wikiUrl")
    private String wikiUrl;

    @Column(name = "race")
    private String race;

    @Column(name = "birth")
    private String birth;

    @Column(name = "gender")
    private String gender;

    @Column(name = "death")
    private String death;

    @Column(name = "hair")
    private String hair;

    @Column(name = "height")
    private String height;

    @Column(name = "realm")
    private String realm;

    @Column(name = "spouse")
    private String spouse;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.wikiUrl);
        hash = 79 * hash + Objects.hashCode(this.race);
        hash = 79 * hash + Objects.hashCode(this.birth);
        hash = 79 * hash + Objects.hashCode(this.gender);
        hash = 79 * hash + Objects.hashCode(this.death);
        hash = 79 * hash + Objects.hashCode(this.height);
        hash = 79 * hash + Objects.hashCode(this.realm);
        hash = 79 * hash + Objects.hashCode(this.spouse);

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Characteres other = (Characteres) obj;

        if (!Objects.equals(this.name, other.name)) {
            return false;
        }

        if (!Objects.equals(this.wikiUrl, other.wikiUrl)) {
            return false;
        }
        if (!Objects.equals(this.birth, other.birth)) {
            return false;
        }
        if (!Objects.equals(this.death, other.death)) {
            return false;
        }
        if (!Objects.equals(this.gender, other.gender)) {
            return false;
        }
        if (!Objects.equals(this.hair, other.hair)) {
            return false;
        }
        if (!Objects.equals(this.height, other.height)) {
            return false;
        }
        if (!Objects.equals(this.race, other.race)) {
            return false;
        }
        if (!Objects.equals(this.realm, other.realm)) {
            return false;
        }
        if (!Objects.equals(this.spouse, other.spouse)) {
            return false;
        }

        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("City{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", name='").append(wikiUrl).append('\'');
        sb.append(", name='").append(race).append('\'');
        sb.append(", name='").append(birth).append('\'');
        sb.append(", name='").append(gender).append('\'');
        sb.append(", name='").append(death).append('\'');
        sb.append(", name='").append(height).append('\'');
        sb.append(", name='").append(realm).append('\'');
        sb.append(", name='").append(spouse).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
