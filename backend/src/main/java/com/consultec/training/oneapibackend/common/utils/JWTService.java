package com.consultec.training.oneapibackend.common.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
@Getter
@Setter
public class JWTService {

    private String SECRET_KEY = "secret";
    private long TIME_MILLIS = 300000;

    public String extractIdUser(String token) {

        return extractClaim(token).getSubject();

    }

    private Claims extractClaim(String token) {

        final Claims claims = extractAllClaims(token);
        return claims;
    }

    private Claims extractAllClaims(String token) {
        // TODO Auto-generated method stub
        return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {

        return extractExpiration(token).before(new Date());
    }

    public Date extractExpiration(String token) {

        // TODO Auto-generated method stub
        return extractClaim(token).getExpiration();
    }

    public String generateToken(Long idUser) {

        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, idUser);
    }

    private String createToken(Map<String, Object> claims, Long idUser) {

        return Jwts.builder().setClaims(claims).setSubject(String.valueOf(idUser)).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + TIME_MILLIS))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    public Boolean validateToken(String token) {
        return (!isTokenExpired(token));
    }

}
