package com.consultec.training.oneapibackend.repository.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Data
@Entity
@Table(name = "movies")
public class Movies implements Serializable {

    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "runtimeInMinutes")
    private String runtimeInMinutes;

    @Column(name = "budgetInMillions")
    private Double budgetInMillions;

    @Column(name = "boxOfficeRevenueInMillions")
    private Double boxOfficeRevenueInMillions;

    @Column(name = "academyAwardNominations")
    private BigDecimal academyAwardNominations;

    @Column(name = "academyAwardWins")
    private BigDecimal academyAwardWins;

    @Column(name = "rottenTomatesScore")
    private Double rottenTomatesScore;

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 79 * hash + Objects.hashCode(this.id);
        hash = 79 * hash + Objects.hashCode(this.name);
        hash = 79 * hash + Objects.hashCode(this.runtimeInMinutes);
        hash = 79 * hash + Objects.hashCode(this.budgetInMillions);
        hash = 79 * hash + Objects.hashCode(this.boxOfficeRevenueInMillions);
        hash = 79 * hash + Objects.hashCode(this.academyAwardNominations);
        hash = 79 * hash + Objects.hashCode(this.academyAwardWins);
        hash = 79 * hash + Objects.hashCode(this.rottenTomatesScore);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Movies other = (Movies) obj;

        if (!Objects.equals(this.name, other.name)) {
            return false;
        }

        if (!Objects.equals(this.runtimeInMinutes, other.runtimeInMinutes)) {
            return false;
        }
        if (!Objects.equals(this.budgetInMillions, other.budgetInMillions)) {
            return false;
        }
        if (!Objects.equals(this.boxOfficeRevenueInMillions, other.boxOfficeRevenueInMillions)) {
            return false;
        }
        if (!Objects.equals(this.academyAwardNominations, other.academyAwardNominations)) {
            return false;
        }
        if (!Objects.equals(this.academyAwardWins, other.academyAwardWins)) {
            return false;
        }
        if (!Objects.equals(this.rottenTomatesScore, other.rottenTomatesScore)) {
            return false;
        }

        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("City{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", name='").append(runtimeInMinutes).append('\'');
        sb.append(", name='").append(budgetInMillions).append('\'');
        sb.append(", name='").append(boxOfficeRevenueInMillions).append('\'');
        sb.append(", name='").append(academyAwardNominations).append('\'');
        sb.append(", name='").append(academyAwardWins).append('\'');
        sb.append(", name='").append(rottenTomatesScore).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
