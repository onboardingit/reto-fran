package com.consultec.training.oneapibackend.controller.model;

import java.util.Objects;
import com.consultec.training.oneapibackend.controller.model.Book;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.v3.oas.annotations.media.Schema;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * Entity that contains the chapter entity of a book.
 */
@Schema(description = "Entity that contains the chapter entity of a book.")
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.v3.generators.java.SpringCodegen", date = "2021-02-11T07:25:54.642734-05:00[America/Bogota]")


public class Chapter   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("chapterName")
  private String chapterName = null;

  @JsonProperty("book")
  private Book book = null;

  public Chapter id(String id) {
    this.id = id;
    return this;
  }

  /**
   * Chapter identifier number.
   * @return id
   **/
  @Schema(example = "5cd99d4bde30eff6ebccfe9e", description = "Chapter identifier number.")
  
    public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Chapter chapterName(String chapterName) {
    this.chapterName = chapterName;
    return this;
  }

  /**
   * Name or title the chapter in the book.
   * @return chapterName
   **/
  @Schema(example = "A Long-expected Party", description = "Name or title the chapter in the book.")
  
    public String getChapterName() {
    return chapterName;
  }

  public void setChapterName(String chapterName) {
    this.chapterName = chapterName;
  }

  public Chapter book(Book book) {
    this.book = book;
    return this;
  }

  /**
   * Get book
   * @return book
   **/
  @Schema(description = "")
  
    @Valid
    public Book getBook() {
    return book;
  }

  public void setBook(Book book) {
    this.book = book;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Chapter chapter = (Chapter) o;
    return Objects.equals(this.id, chapter.id) &&
        Objects.equals(this.chapterName, chapter.chapterName) &&
        Objects.equals(this.book, chapter.book);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, chapterName, book);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Chapter {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    chapterName: ").append(toIndentedString(chapterName)).append("\n");
    sb.append("    book: ").append(toIndentedString(book)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
