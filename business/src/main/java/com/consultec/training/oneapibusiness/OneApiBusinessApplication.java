package com.consultec.training.oneapibakend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneApiBakendApplication {

    public static void main(String[] args) {
        SpringApplication.run(OneApiBakendApplication.class, args);
    }

}
